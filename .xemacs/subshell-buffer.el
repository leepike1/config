;; M-x proof-top 
;; To travel to the top of the current proof.
(fset 'proof-top
   "\C-rACL2 \C-r\C-x")

;; Support for sending buffer regions to a subprocess shell buffer, such
;; as *shell* or *scheme*. It binds several function keys.
(defun send-to-terminal-p ()
  "Returns true if we are targeting a terminal-window sub-process."
  (get-buffer "*terminal*"))

(defun cur-shell-buffer ()
  "Returns the sub-process buffer for the current window. A terminal
   window has priority over a shell window."
  (if (send-to-terminal-p)
      "*terminal*"
    "*shell*"))

(defun begin-end-points (begin-command end-command)
  "Performs (begin-command), then performs (end-command), and returns
   the points travelled to by those commands as a pair. Before returning
   the pair, this function restores the original point and mark."
  (save-excursion
     (funcall begin-command)
     (let ((begin-end-points-begin (point)))
     (funcall end-command)
     (let ((begin-end-points-end (point)))
     (cons begin-end-points-begin begin-end-points-end)))))

(defun line-points ()
  "Returns a pair containing the start and end positions of the current line"
  (begin-end-points 'beginning-of-line 'end-of-line))

(defun paragraph-points ()
  "Returns a pair containing the start and end positions of the current
   paragraph. If the point is between paragraphs, then this function
   returns the paragraph before the point."
  (begin-end-points 'backward-paragraph 'forward-paragraph))

(defun move-shell-point-to-end ()
  "Moves the shell buffer's point to the end of the buffer."
  (interactive)
  (save-selected-window
    (select-window (get-buffer-window (cur-shell-buffer) t))
    (set-buffer (cur-shell-buffer))
    (goto-char (point-max))))

(defun exec-shell-command ()
  "Executes the string inserted into shell after the last shell
   prompt."
  (interactive)
  (save-excursion
    (set-buffer (cur-shell-buffer))
    (if (send-to-terminal-p)
	(insert-shell-string "\n")
       (comint-send-input))
  (move-shell-point-to-end)))

(defun insert-shell-string (string)
  "Inserts STRING at the end of the shell buffer and moves
   the shell's point after the inserted string."
  (move-shell-point-to-end)
  (insert-string string (cur-shell-buffer)))

(defun insert-shell-points (begin-point end-point)
  "Appends the input given by the function's arguments  to the current shell buffer,
   and moves the shell's point after the inserted string."
  (interactive)
  (move-shell-point-to-end) 
  (append-to-buffer (cur-shell-buffer) begin-point end-point))

(defun send-string-to-shell (str)
  "Appends the argument string to the current shell buffer, and then invokes the shell process
   with the sent string. The current shell buffer's curser will be at the end of the buffer"
  (interactive)
  (insert-shell-string str)
  (exec-shell-command))

(defun send-points-to-shell (begin-point end-point)
  "Appends the input given by the arguments to the current shell
   buffer, and then invokes the shell process. The current shell buffer's
   curser will be at the end of the buffer."
  (interactive)
  (insert-shell-points begin-point end-point)
  (exec-shell-command))

(defun send-line-to-shell ()
  "Sends the current line to the shell and executes it"
  (interactive)
  (let ((cur-line (line-points)))
    (send-points-to-shell (car cur-line) (cdr cur-line))))

(defun send-paragraph-to-shell ()
  "Copies the current line to the shell and executes it."
  (interactive)
  (let ((cur-paragraph (paragraph-points)))
    (send-points-to-shell (car cur-paragraph) (cdr cur-paragraph))))

(defun send-region-to-shell ()
  "Copies the current region to the shell and executes it."
  (interactive)
  (send-points-to-shell (region-beginning) (region-end)))

(defun send-undo-to-shell ()
  "Sends 'undo ();' to the shell and executes it."
  (interactive)
  (send-string-to-shell "undo ();"))

(defun send-pr-to-shell ()
  "Sends 'pr ();' to the shell and executes it."
  (interactive)
  (send-string-to-shell "pr ();"))

(defun send-ubt-to-shell ()
  "Extracts the rune of the event being defined in the
   current paragraph and executes ':ubt <rune-name>' to the shell."
  (interactive)
  (save-excursion
    (backward-paragraph)
    (search-forward "(def" nil nil 1)
    (skip-syntax-forward "w_")
    (skip-syntax-forward " ")
    (let ((name-begin-point (point)))
    (skip-syntax-forward "w_")
    (let ((name-end-point (point)))
    (insert-shell-string ":ubt! ")
    (send-points-to-shell name-begin-point name-end-point)))))

(defun hint-acl2 ()
  "Adds a :hint to an ACL2 defthm form."
  (interactive)
  (forward-paragraph)
  (backward-char)
  (backward-char)
  (newline-and-indent)
  (insert-string ":hints ((\"Goal\" ))")
  (backward-char)
  (backward-char))

(global-set-key [f5] 'send-line-to-shell)
(global-set-key [f6] 'send-paragraph-to-shell)
(global-set-key [f7] 'send-ubt-to-shell)
(global-set-key [f8] 'send-region-to-shell)

(make-local-variable 'paragraph-start)
;; Having `^' is not clean, but page-delimiter
;; has them too, and removing those is a pain.
(setq paragraph-start "[\n]")
