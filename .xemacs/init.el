;; ------------------------------------------------------------
;; Are we running XEmacs or Emacs?
(defvar running-xemacs (string-match "XEmacs\\|Lucid" emacs-version))
(setq load-path (cons "~/.xemacs" load-path))

(require 'package)
(add-to-list 'package-archives
  '("melpa-stable" . "http://stable.melpa.org/packages/") t)
(package-initialize)

;; ------------------------------------------------------------
;; Haskell

'(haskell-check-command "hlint")
'(haskell-hoogle-command nil)
'(haskell-stylish-on-save t)
'(haskell-tags-on-save t)
(add-hook 'haskell-mode-hook 'haskell-indentation-mode)
(add-hook 'haskell-mode-hook 'turn-on-haskell-simple-indent)
(add-hook 'haskell-mode-hook 'turn-on-haskell-doc-mode)

(when (fboundp 'electric-indent-mode) (electric-indent-mode -1))
;; Disable electric-indent-mode, which is enabled by default in Emacs
;; 24.4, and is very annoying haskell-mode (e.g. it attempts to indent
;; every top-level definition.
(add-hook 'haskell-mode-hook '(lambda () (electric-indent-mode 0)))

;; Unicode symbols
(defvar haskell-font-lock-symbols)
(setq haskell-font-lock-symbols t)

;; ------------------------------------------------------------
;; SAL

(autoload 'sal-mode "sal" "Major mode for SAL." t)
(autoload 'run-sal "sal" "Switch to interactive SAL buffer." t)
(setq auto-mode-alist (cons '("\\.sal" . sal-mode) auto-mode-alist))

;; ------------------------------------------------------------
;; Latex

; Turns on reftex mode when running latex mode.  Check out reftex and
; all of its wonderful features!!
(add-hook 'latex-mode-hook 'turn-on-reftex)
(setq auto-mode-alist (cons '("\\.tex" . latex-mode) auto-mode-alist))

;; ------------------------------------------------------------
;; Agda

;; (load-file (let ((coding-system-for-read 'utf-8))
;;                (shell-command-to-string "agda-mode locate")))

;; ------------------------------------------------------------
;; Magit

(setq magit-last-seen-setup-instructions "1.4.0")
(global-set-key (kbd "\C-xm") 'magit-status)

;; ------------------------------------------------------------
;; Markdown

(autoload 'markdown-mode "markdown-mode.el" "Major mode for editing Markdown files" t)
(setq auto-mode-alist (cons '("\\.md" . markdown-mode) auto-mode-alist))

;; ------------------------------------------------------------
;; Backups

;; No.
(setq make-backup-files nil)

;; ------------------------------------------------------------
;; Text modes

(add-hook 'text-mode-hook 'turn-spell-checking-on)
(add-hook 'text-mode-hook 'visual-line-mode)

;; ------------------------------------------------------------
;; Global settings

;; Show columns
(setq column-number-mode t)

;; Tabs are spaces
(setq-default indent-tabs-mode nil)

;; Show only trailing whitespace
(setq-default show-trailing-whitespace t)

; Turns on the visible bell--this makes the screen flash when you've been
; naughty or when you have an appointment.
(setq visible-bell t)

;; Set up the keyboard so the delete key on both the regular keyboard
;; and the keypad delete the character under the cursor and to the right
;; under X, instead of the default, backspace behavior.
(global-set-key [delete] 'delete-char)
(global-set-key [kp-delete] 'delete-char)

;; Visual feedback on selections
(setq-default transient-mark-mode t)

;; Always end a file with a newline
(setq require-final-newline t)

;; Stop at the end of the file, not just add lines
(setq next-line-add-newlines nil)

;; Sets the default major mode that emacs starts up in.
(setq default-major-mode 'text-mode)

(require 'package)
(add-to-list 'package-archives
  '("melpa" . "http://melpa.milkbox.net/packages/") t)
(package-initialize)

;; (require 'helm-config)

;; ------------------------------------------------------------
;; Global definitions

;; Unfill functions (for "MS Word"-ing text)
(defun unfill-paragraph ()
      (interactive)
      (let ((fill-column (point-max)))
        (fill-paragraph nil)))

;; Flyspell
(defun turn-spell-checking-on ()
  "Turn speck-mode or flyspell-mode on."
  (flyspell-mode 1)
  )

;; ------------------------------------------------------------
;; Global key bindings

;; Run align-regexp (in any mode)
(global-set-key (kbd "C-c C-g") 'align-regexp)

;; unfill-paragraph locally defined
(global-set-key (kbd "C-M-q") 'unfill-paragraph)

; For commenting and uncommenting quickly
(global-set-key (kbd "\C-xu") 'uncomment-region)
(global-set-key (kbd "\C-xc") 'comment-region)

(global-set-key (kbd "\C-xri") 'string-insert-rectangle)

;; -------------------------------------
;; Helm stuff
;; (helm-mode 1)
;; (global-set-key (kbd "M-x") 'helm-M-x)
;; (global-set-key (kbd "C-x C-f") 'helm-find-files)

;; ----------------------------------------
;; Ivy

(ivy-mode 1)

(setq ivy-use-virtual-buffers t)
(setq ivy-count-format "(%d/%d) ")
