- http://www.emacswiki.org/emacs/WindowResize
    - C-x { shrink horizontally
    - C-x } grow horizontally

- Rectangle commmands
    - C-x r k kill rectangle

- Turn off auto-fill:
    - M-x auto-fill-mode

- Repeat:
    - C-x z (then z over and over)

- Split:
    - C-x 3 (three screens)
    - C-x { (or }) adjust.  Use with repeat.
